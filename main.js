var Table = require('./table');
var Client = require('./client');
var Bar = require('./bar');
var Waiter = require('./waiter');

// Crea los ojetos basicos para la ejecucion
function factory(){

  this.arrayTable = [];
  this.arrayClient = [];
  this.waiter = new Waiter('Mesonero');
  this.bar = new Bar('bar');

  // Crea todas las mesas del restaurante
  for (var i = 0; i < 3; i++) {
    arrayTable[i] = new Table(i);
  }

  //Crea todos los clientes
  for (var i = 0; i < 6; i++) {
    arrayClient[i] = new Client('Cliente'+i)
  }

}

// Detieene la ejecucion si se cumple la condiccion
function stop(){
  if (waiter.existClient(arrayClient) === true && waiter.status === "En espera" && waiter.existClientInTable(arrayTable) === false){
    clearInterval(setMain);
  }
};

// Muestra la cola de clientes (solo los primeros 5)
function showClientsInTail(){
  var cont = 0;
  var first;
  console.log("####   Clientes en cola: ####");
  while (cont < 5 && cont < arrayClient.length) {
    first = cont === 0 && waiter.status === "Buscando Cliente" ? " d(-_-)b " : ""
    console.log(arrayClient[cont].name + first );
    cont = cont + 1
  }
}

// Muestra la informacion de las mesas
function showTable(){
  var clientInTable;
  var attWaiter;
  console.log("############");
  for (var i = 0; i < arrayTable.length; i++) {
    if (arrayTable[i].status == "Ocupada") {
      clientInTable = " | Cliente: " + arrayTable[i].client.name + "| Estatus: " + arrayTable[i].client.status;
      if (arrayTable[i].client.status === "A la mesa" || (arrayTable[i].client.status === "Atendido" && waiter.status === "go bar")  ) {
        attWaiter = " d(-_-)b ";
      } else if (arrayTable[i].client.status === "Ordenando") {
        attWaiter = " ☝"
      } else if (arrayTable[i].client.status === "Comiendo" && waiter.status === "Atendiendo") {
        attWaiter = " d(-_-)b --> |@| ";
      } else if (arrayTable[i].client.status === "Comiendo") {
        attWaiter = " |@| ";
      }else{
        attWaiter = "";
      }
    }else{
      clientInTable = "";
      attWaiter = "";
    }
    console.log("Mesa: "+ arrayTable[i].name + " | Estatus: " + arrayTable[i].status + clientInTable + attWaiter);
  }
}

// Muestra la informacion del bar
function showBar(){
  var waiterInBar = waiter.status === "Sirviendo" ? " d(-_-)b " : "";
  console.log('#######');
  console.log("Bar: |@||@||@||@||@||@||@||@||@||@||@||@||@||@| " + waiterInBar);
}

function show(){
  process.stdout.write('\033c') // Borra la pantalla
  waiter.show();
  showClientsInTail();
  showTable();
  showBar();
}

function main(){
  show();
  if ((waiter.status === "En espera" && waiter.tableFree(arrayTable) === true && waiter.existClient(arrayClient) === false) || (waiter.status === "Sentando Cliente" && waiter.tableFree(arrayTable) === true && waiter.existClient(arrayClient) === false) ) {
    waiter.searchClient();
  } else if (waiter.status === "Buscando Cliente") {
    if (waiter.existClient(arrayClient) === false) {
      firstClient = arrayClient.shift();
      waiter.sitClient( firstClient, waiter.tableAvailable(arrayTable));
      if (waiter.tableFree(arrayTable) === false) {
        waiter.status = "Atendiendo";
      }
      setTimeout(function(){ firstClient.toOrder(); }, 3000);
    }
  } else if (waiter.searchClientOrder(arrayTable) === true && waiter.status === "Atendiendo" ) {
    waiter.searchTableClientOrder(arrayTable);
  } else if (waiter.status === "go bar") {
    waiter.status = "Sirviendo";
  } else if (waiter.status === "Sirviendo"){
    clientAttended = waiter.searchClientAttended(arrayTable);
    setTimeout(function(){ clientAttended.client.finishMeal(); }, 6000);
  } else if (waiter.status === "Atendiendo") {
    setTimeout(function(){
      for (var i = 0; i < arrayTable.length; i++) {
        arrayTable[i].clientToLeave();
      }
    }, 6000);
    waiter.status = "En espera";
  };

}

var setMain = setInterval(function(){
  main()
  stop()
},2000)

factory();
