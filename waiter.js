function Waiter(name){
  this.name = name;
  this.status = "En espera";
}

// En uso
// Sienta a un cliente
Waiter.prototype.sitClient = function (client, tableAvailable) {
  tableAvailable.status = "Ocupada";
  tableAvailable.client = client;
  client.status = "A la mesa";
  this.status = "Sentando Cliente";
};

// En uso
// Muestra al mesonero en sitio de espera
Waiter.prototype.show = function(){
  if (this.status == "En espera") {
    console.log("d(-_-)b zZzZzZzZzZzZ");
  }
};

// En uso
// Metodo que pregunta si hay mesa disponible, devuelve true o false
Waiter.prototype.tableFree = function(arrayTable){
  var cond = false;
  var cont = 0;
  do {
    if (arrayTable[cont].status === "Vacia") {
      cond = true;
    }else{
      cont = cont + 1;
    }
  } while (cond === false && cont < arrayTable.length);
  return cond;
}

// En uso
// Metodo que encuentra la mesa disponible
Waiter.prototype.tableAvailable = function(arrayTable){
  var cond = false;
  var cont = 0;
  do {
    if (arrayTable[cont].status === "Vacia") {
      cond = true;
    }else{
      cont = cont + 1;
    }
  } while (cond === false);
  return arrayTable[cont];
}

// En uso
// Metodo que modifica el status a buscando cliente
Waiter.prototype.searchClient = function(){
  this.status = "Buscando Cliente";
}

// En uso
// Metodo que verifica si hay clientes con ordenes
Waiter.prototype.searchClientOrder = function (arrayTable){
  var cond = false;
  var cont = 0;

  do {
    if (arrayTable[cont].status === "Ocupada") {
      if (arrayTable[cont].client.status === "Ordenando") {
        cond = true;
      }
    }
    cont = cont + 1;
  } while (cond == false && cont < arrayTable.length);
  return cond;
}

// En uso
// Busca si hay cliente con orden
Waiter.prototype.searchTableClientOrder = function (arrayTable){
  var cond = false;
  var cont = 0;
  var tableOrder;
  do {
    if (arrayTable[cont].status === "Ocupada") {
      if (arrayTable[cont].client.status === "Ordenando") {
        tableOrder = arrayTable[cont];
        arrayTable[cont].client.status = "Atendido";
        this.status = "go bar";
        cond = true;
      }
    }
    cont = cont + 1;
  } while (cond == false && cont < arrayTable.length);
  return tableOrder;
}

// En uso
// Busca al cliente que fue atendido
Waiter.prototype.searchClientAttended = function (arrayTable){
  var cond = false;
  var cont = 0;
  var client;

  do {
    if (arrayTable[cont].client.status === "Atendido") {
      arrayTable[cont].client.status = "Comiendo";
      this.status = "Atendiendo";
      cond = true;
      client = arrayTable[cont];
    }
    cont = cont + 1;
  } while (cond == false && cont < arrayTable.length);
  return client;
}

// En uso
// Metodo que verifica si todas las mesas estan listas para irse
Waiter.prototype.searchClientsToLeave = function(arrayTable){
  var contLeave = 0,  cont = 0, contBusy = 0, cond = false;

  do {
    if (arrayTable[cont].status === "Ocupada"){
      contBusy ++;
      if (arrayTable[cont].client.status == "Listo para irme"){
        contLeave ++;
      }
    }
    cont ++;
  } while (cont < arrayTable.length);

  if (contLeave === contBusy) {
    cond = true;
  }

  return cond;
}

Waiter.prototype.existClient = function(arrayclient){
  var cond = false;
  if (typeof arrayclient[0] === 'undefined') {
   cond = true;
  }
  return cond;
}

Waiter.prototype.existClientInTable = function(arrayTable){
  var cond = false;
  var cont = 0;
  do {
    if (arrayTable[cont].status === "Ocupada") {
      cond = true;
    }
    cont ++;
  } while (cond === false && cont < arrayTable.length);
  return cond;
}





module.exports = Waiter;
