function Client(name){
  this.name = name;
  this.status = "En cola";
}

Client.prototype.toOrder = function () {
  this.status = "Ordenando";
};

Client.prototype.finishMeal = function () {
  this.status = "Listo para irme";
};

Client.prototype.show = function(){
  console.log(this.name + " " + this.status);
}

module.exports = Client;
